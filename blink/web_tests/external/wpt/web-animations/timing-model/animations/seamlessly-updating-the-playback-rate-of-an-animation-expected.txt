This is a testharness.js-based test.
FAIL Updating the playback rate maintains the current time promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
FAIL Updating the playback rate while running makes the animation pending promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
FAIL Updating the playback rate on a play-pending animation maintains the current time promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
FAIL Updating the playback rate on a pause-pending animation maintains the current time promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
FAIL If a pending playback rate is set multiple times, the latest wins promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
FAIL In the idle state, the playback rate is applied immediately animation.updatePlaybackRate is not a function
FAIL In the paused state, the playback rate is applied immediately promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
FAIL Updating the playback rate on a finished animation maintains the current time promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
FAIL Updating the playback rate to zero on a finished animation maintains the current time promise_test: Unhandled rejection with value: object "TypeError: animation.updatePlaybackRate is not a function"
Harness: the test ran to completion.

